# LINK FOR REVIEW VIDEO
https://youtu.be/gaS-bE7PAeQ

# CLONE GIT
using SSH => open terminal, write git clone git@gitlab.com:Happid/inspiro-tech-fe.git
OR using HTTPS => open terminal, write git clone https://gitlab.com/Happid/inspiro-tech-fe.git

# START PROJECT
1. after clone the proejct. open folder using cmd/terminal/vscode, write => npm install
2. start server, write in terminal => npm run server
3. start client server, write in terminal => npm run dev

# MY CONTACT INFO 
happidilmi@gmail.com
081280823716